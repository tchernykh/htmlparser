package com.yabby.htmlparser;

import com.yabby.htmlparser.impl.HtmlNode;

/**
 * Created by chernykh on 3/28/2015.
 */
public interface Action {
    void action(HtmlNode node);
}
