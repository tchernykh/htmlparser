package com.yabby.htmlparser;

import com.yabby.htmlparser.impl.HtmlNode;
import com.yabby.htmlparser.rule.RuleItem;

/**
* Rule which consists of set of items and action which is called if all actions were proceed successfully
*/
public class Rule {
    Action ruleAction;
    RuleItem[] rules;

    public Rule(Action ruleAction, RuleItem... ruleItems) {
        this.ruleAction = ruleAction;
        this.rules = ruleItems;
    }

    public RuleItem get(int index) {
        return rules[index];
    }

    public void run(HtmlNode node) {
        ruleAction.action(node);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (RuleItem rule : rules) {
            sb.append(":").append(rule.getClass().getSimpleName());
        }
        return "Rule of " + rules.length + " items" + sb ;
    }
}