package com.yabby.htmlparser.impl.neko;

import com.yabby.htmlparser.impl.HtmlNode;
import org.xml.sax.Attributes;

/**
 * HtmlNode implementation of NekoHtml parser
 */
public class NekoHtmlNode implements HtmlNode {
    private String name;
    private Attributes attributes;

    public NekoHtmlNode(String name, Attributes attributes) {
        this.name = name;
        this.attributes = attributes;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getAttr(String name) {
        return attributes.getValue(name);
    }
}
