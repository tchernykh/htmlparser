package com.yabby.htmlparser.impl.neko;

import com.yabby.htmlparser.NodeVisitor;
import com.yabby.htmlparser.impl.HtmlNode;
import com.yabby.htmlparser.impl.Parser;
import org.apache.xerces.parsers.AbstractSAXParser;
import org.apache.xerces.xni.parser.XMLParserConfiguration;
import org.cyberneko.html.HTMLConfiguration;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.io.InputStream;
import java.util.Stack;

/**
 * Parses html using Neko SAX parser
 */
public class NekoParser implements Parser {

    public static class NekoSaxParser extends AbstractSAXParser {
        protected NekoSaxParser(XMLParserConfiguration config) {
            super(config);
        }
    }

    @Override
    public void parse(InputStream stream, NodeVisitor visitor) throws Exception {
        XMLParserConfiguration config = new HTMLConfiguration();
        config.setFeature("http://cyberneko.org/html/features/augmentations", true);
        config.setProperty("http://cyberneko.org/html/properties/names/elems", "lower");
        NekoSaxParser parser = new NekoSaxParser(config);
        parser.setContentHandler(new NekoHandler(visitor));
        parser.parse(new InputSource(stream));
    }

    public static class NekoHandler extends DefaultHandler {

        private NodeVisitor visitor;

        Stack<NekoHtmlNode> state = new Stack<>();

        int ignoredCount = 0;

        public NekoHandler(NodeVisitor visitor) {
            this.visitor = visitor;
        }

        @Override
        public void startElement(String uri, String localName, String name, Attributes a) {
            NekoHtmlNode node = new NekoHtmlNode(name, a);
            state.push(node);
            if (ignoredCount > 0 || !this.visitor.in(node)) {
                ignoredCount++;
            }
        }

        @Override
        public void endElement(String uri, String localName, String qName) throws SAXException {
            HtmlNode node = state.pop();
            if (ignoredCount > 0) {
                ignoredCount--;
            } else {
                this.visitor.out(node);
            }
        }
    };
}
