package com.yabby.htmlparser.impl;

/**
 * Single html node like div, head, img with attrubutes
 */
public interface HtmlNode {
    String getName();

    String getAttr(String name);
}
