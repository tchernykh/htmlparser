package com.yabby.htmlparser.impl.jericho;

import com.yabby.htmlparser.NodeVisitor;
import com.yabby.htmlparser.impl.HtmlNode;
import com.yabby.htmlparser.impl.Parser;
import net.htmlparser.jericho.*;

import java.io.InputStream;
import java.util.Stack;

/**
 * Parser that uses Jericho library to parse
 * TODO end tags ignored
 */
public class JerichoParser implements Parser {

    private boolean ignorePlainTags = true;

    public void setIgnorePlainTags(boolean ignorePlainTags) {
        this.ignorePlainTags = ignorePlainTags;
    }

    @Override
    public void parse(InputStream stream, NodeVisitor visitor) throws Exception {
        StreamedSource source = new StreamedSource(stream);
        Stack<Tag> ignoredNodes = new Stack<>();
        Stack<HtmlNode> state = new Stack<>();
        for (Segment segment : source) {
            if (segment instanceof StartTag) {
                Tag tag = (Tag) segment;
                HtmlNode node = new JerichoHtmlNode(tag);
                state.push(node);
                if (!ignorePlainTags || !tag.getName().startsWith("!")) {
                    if (!ignoredNodes.isEmpty() || !visitor.in(node)) {
                        ignoredNodes.add(tag);
                    }
                }
            } else if (segment instanceof EndTag) {
                HtmlNode node = state.pop();
                if (!ignoredNodes.isEmpty()) {
                    //noinspection StatementWithEmptyBody
                    while (!((EndTag) segment).getName().equals(ignoredNodes.pop().getName()));
                } else {
                    visitor.out(node);
                }
            }
//
//            } else if (segment instanceof CharacterReference) {
//                CharacterReference characterReference=(CharacterReference)segment;
//            } else {
//            }
        }
    }
}
