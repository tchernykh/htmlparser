package com.yabby.htmlparser.impl.jericho;

import com.yabby.htmlparser.impl.HtmlNode;
import net.htmlparser.jericho.Attribute;
import net.htmlparser.jericho.Attributes;
import net.htmlparser.jericho.Tag;

/**
 * HtmlNode implementation for jericho parser
 */
public class JerichoHtmlNode implements HtmlNode {

    private Tag tag;
    private Attributes attributes;

    public JerichoHtmlNode(Tag tag) {
        this.tag = tag;
    }

    @Override
    public String getName() {
        return tag.getName();
    }

    @Override
    public String getAttr(String name) {
        if (this.attributes == null) {
            this.attributes = tag.parseAttributes();
        }
        Attribute attr = attributes == null? null: attributes.get(name);
        return attr == null? null: attr.getValue();
    }

    @Override
    public String toString() {
        return tag.getName() + "(" + tag.parseAttributes().toString() + ")";
    }
}
