package com.yabby.htmlparser.impl;

import com.yabby.htmlparser.NodeVisitor;

import java.io.InputStream;

/**
 * Created by chernykh on 3/28/2015.
 */
public interface Parser {

    void parse(InputStream stream, NodeVisitor nodeVisitor) throws Exception;
}
