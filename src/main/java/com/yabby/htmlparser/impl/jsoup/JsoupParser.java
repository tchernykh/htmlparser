package com.yabby.htmlparser.impl.jsoup;

import com.yabby.htmlparser.NodeVisitor;
import com.yabby.htmlparser.impl.Parser;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Node;

import java.io.InputStream;

public class JsoupParser implements Parser {

    private boolean ignorePlainTags = true;

    public void setIgnorePlainTags(boolean ignorePlainTags) {
        this.ignorePlainTags = ignorePlainTags;
    }

    public void parse(InputStream stream, NodeVisitor visitor) throws Exception {
        Document doc = Jsoup.parse(stream, "UTF-8", "");
        for (int i = 0; i < doc.childNodeSize(); i++) {
            Node node = doc.childNode(i);
            if ("html".equals(node.nodeName())) {
                processNode(node, visitor);
                break;
            }
        }
    }

    public void processNode(Node top, NodeVisitor visitor) {
        JsoupHtmlNode htmlNode = new JsoupHtmlNode(top);
        if (visitor.in(htmlNode)) {
            for (int i = 0; i < top.childNodeSize(); i++) {
                Node node = top.childNode(i);
                if (node.nodeName().startsWith("#")) {
                    if (!ignorePlainTags) {
                        JsoupHtmlNode innerHtmlNode = new JsoupHtmlNode(node);
                        visitor.in(innerHtmlNode);
                        visitor.out(innerHtmlNode);
                    }
                } else {
                    processNode(node, visitor);
                }
            }
            visitor.out(htmlNode);
        }
    }
}
