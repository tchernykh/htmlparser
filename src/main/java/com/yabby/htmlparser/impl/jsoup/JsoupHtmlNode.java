package com.yabby.htmlparser.impl.jsoup;

import com.yabby.htmlparser.impl.HtmlNode;
import org.jsoup.nodes.Node;

/**
 * HtmlNode implementation for jsoup parser
 */
public class JsoupHtmlNode implements HtmlNode {

    private Node node;

    public JsoupHtmlNode(Node node) {
        this.node = node;
    }

    @Override
    public String getName() {
        return node.nodeName();
    }

    @Override
    public String getAttr(String name) {
        return node.attributes().hasKey(name)? node.attributes().get(name): null;
    }

    @Override
    public String toString() {
        return node.nodeName() + "(" + node.attributes().html() + ")";
    }
}
