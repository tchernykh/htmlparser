package com.yabby.htmlparser;

import com.yabby.htmlparser.impl.HtmlNode;

/**
 * internal visitor to walk through html tree
 */
public interface NodeVisitor {
    boolean in(HtmlNode node);
    void out(HtmlNode node);
}
