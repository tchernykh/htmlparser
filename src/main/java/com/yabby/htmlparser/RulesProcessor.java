package com.yabby.htmlparser;

import com.yabby.htmlparser.impl.HtmlNode;
import com.yabby.htmlparser.rule.CheckResult;
import com.yabby.htmlparser.rule.RuleItem;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * Class that handles rules workflow across html tree processing.
 */
public class RulesProcessor implements NodeVisitor {

    private static final boolean DEBUG = true;

    private static class RuleState {
        Rule rule;
        int currentIndex;

        public RuleState(Rule rule, int currentIndex) {
            this.rule = rule;
            this.currentIndex = currentIndex;
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < rule.rules.length ; i++) {
                if (i == currentIndex) {
                    sb.append(">>>");
                }
                sb.append(rule.rules[i]);
                if (i == currentIndex) {
                    sb.append("<<<");
                }
                if (rule.rules.length != i +1) {
                    sb.append(" - ");
                }
            }
            return "State: " + sb;
        }
    }

    private Stack<List<RuleState>> rulesStack = new Stack<>();

    public RulesProcessor(List<Rule> rules) {
        List<RuleState> state = new ArrayList<>(rules.size());
        for (Rule rule : rules) {
            state.add(new RuleState(rule, 0));
        }
        rulesStack.push(state);
    }

    @Override
    public boolean in(HtmlNode node) {
//        System.out.println("in " + node.getName() + " id: " + node.getAttr("id"));

        List<RuleState> currentRules = rulesStack.peek();
        if (DEBUG) {
            System.out.println("Processing " + node + ":");
            for (RuleState state : currentRules) {
                System.out.println("\tState: " + state);
            }
        }

        List<RuleState> nextRules = new ArrayList<>();

        for (RuleState state : currentRules) {
            RuleItem ruleItem = state.rule.get(state.currentIndex);
            CheckResult checkResult = ruleItem.check(null, node);
            if (checkResult.isRunAction() && state.rule.rules.length == state.currentIndex + 1) {
                System.out.println("run action on " + ruleItem);
                state.rule.run(node);
            }
            if (checkResult.isContinueWithCurrentRule()) {
                nextRules.add(state);
            }
            if (checkResult.isGoFurther() && state.rule.rules.length > state.currentIndex + 1) {
                nextRules.add(new RuleState(state.rule, state.currentIndex + 1));
            }
        }
        if (nextRules.isEmpty()) return false;
        rulesStack.push(nextRules);
        return true;
    }

    @Override
    public void out(HtmlNode node) {
//        System.out.println("out " + node.getName() + " id: " + node.getAttr("id"));
        rulesStack.pop();
    }
}
