package com.yabby.htmlparser;

/**
 * State of rule. This changes during processing.
 */
public class RuleState implements Cloneable {
    int currentIndex = 0;
    boolean enabled = true;

    @SuppressWarnings({"CloneDoesntCallSuperClone", "CloneDoesntDeclareCloneNotSupportedException"})
    @Override
    protected RuleState clone() {
        RuleState res = new RuleState();
        res.currentIndex = currentIndex;
        res.enabled = enabled;
        return res;
    }
}
