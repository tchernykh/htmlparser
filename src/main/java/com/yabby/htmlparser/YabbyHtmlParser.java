package com.yabby.htmlparser;

import com.yabby.htmlparser.impl.HtmlNode;
import com.yabby.htmlparser.impl.Parser;
import com.yabby.htmlparser.rule.CheckResult;

import java.io.InputStream;
import java.util.*;

/**
 * Html parser that parses html based on set of rules and using provided parser.
 */
public class YabbyHtmlParser {

    private Parser parser;

    private List<Rule> rules;

    public YabbyHtmlParser(Parser parser) {
        this.parser = parser;
        this.rules = new ArrayList<>();
    }

    /**
     * Parse stream
     */
    public ParsingContext parse(InputStream streamToParse) throws Exception {
        this.parser.parse(streamToParse, new RulesProcessor(this.rules));
        return null;
    }

    public void addRule(Rule rule) {
        this.rules.add(rule);
    }

}
