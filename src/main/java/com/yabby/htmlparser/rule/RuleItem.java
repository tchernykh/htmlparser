package com.yabby.htmlparser.rule;

import com.yabby.htmlparser.ParsingContext;
import com.yabby.htmlparser.impl.HtmlNode;

/**
 * Rule that determine parsing logic.
 */
public interface RuleItem {
    CheckResult check(ParsingContext context, HtmlNode node);
}
