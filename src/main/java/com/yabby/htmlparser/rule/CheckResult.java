package com.yabby.htmlparser.rule;

/**
 * Represent result of node check
 */
public enum CheckResult {
    // run action if it's last item and fork rule - proceed with current ruleItem further and proceed with next item
    CONTINUE(true, true, true),
    // run action if it's last item or go to next item
    ACTION(true, true, false),
    // proceed with current
    SKIP(false, false, true),
    // stop execution for this rule
    STOP(false, false, false);

    boolean runAction = false;
    boolean goFurther = false;
    boolean continueWithCurrentRule = false;

    public boolean isRunAction() {
        return runAction;
    }

    public boolean isGoFurther() {
        return goFurther;
    }

    public boolean isContinueWithCurrentRule() {
        return continueWithCurrentRule;
    }

    private CheckResult(boolean runAction, boolean goFurther, boolean continueWithCurrentRule) {
        this.runAction = runAction;
        this.goFurther = goFurther;
        this.continueWithCurrentRule = continueWithCurrentRule;
    }
}
