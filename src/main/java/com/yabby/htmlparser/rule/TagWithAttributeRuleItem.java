package com.yabby.htmlparser.rule;

import com.yabby.htmlparser.ParsingContext;
import com.yabby.htmlparser.impl.HtmlNode;

/**
 * Check for tag with particular attribute
 */
public class TagWithAttributeRuleItem extends TagRuleItem {

    private String attributeName;
    private String attributeValue;

    public TagWithAttributeRuleItem(String tagName, String attributeName, String attributeValue) {
        super(tagName);
        this.attributeName = attributeName;
        this.attributeValue = attributeValue;
    }

    @Override
    public CheckResult check(ParsingContext context, HtmlNode node) {
        if (super.check(context, node) == CheckResult.ACTION && attributeValue.equalsIgnoreCase(node.getAttr(attributeName))) {
            return CheckResult.ACTION;
        } else {
            return CheckResult.STOP;
        }
    }

    @Override
    public String toString() {
        return "TAG[" + tagName + " " + attributeName + "='" + attributeValue + "']";
    }
}
