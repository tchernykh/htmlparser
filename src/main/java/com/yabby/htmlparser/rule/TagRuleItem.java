package com.yabby.htmlparser.rule;

import com.yabby.htmlparser.ParsingContext;
import com.yabby.htmlparser.impl.HtmlNode;

/**
 * Main rule item - verify process logic for current tag and it's attributes.
 */
public class TagRuleItem implements RuleItem {

    // tag name that we're looking for, could be null if we looking any tag with attrs
    protected String tagName;

    public TagRuleItem(String tagName) {
        this.tagName = tagName;
    }

    @Override
    public CheckResult check(ParsingContext context, HtmlNode node) {
        if (tagName == null) {
            return CheckResult.ACTION;
        } else {
            return tagName.equals(node.getName()) ? CheckResult.ACTION: CheckResult.STOP;
        }
    }

    @Override
    public String toString() {
        return "TAG[" + tagName + "]";
    }
}
