package com.yabby.htmlparser.rule;

import com.yabby.htmlparser.ParsingContext;
import com.yabby.htmlparser.impl.HtmlNode;

public class AnyNode implements RuleItem {
    @Override
    public CheckResult check(ParsingContext context, HtmlNode node) {
        return CheckResult.CONTINUE;
    }

    @Override
    public String toString() {
        return "*";
    }
}
