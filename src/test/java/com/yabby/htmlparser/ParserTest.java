package com.yabby.htmlparser;

import com.yabby.htmlparser.impl.HtmlNode;
import com.yabby.htmlparser.impl.Parser;
import com.yabby.htmlparser.impl.jericho.JerichoParser;
import com.yabby.htmlparser.impl.jsoup.JsoupParser;
import com.yabby.htmlparser.impl.neko.NekoParser;
import com.yabby.htmlparser.rule.AnyNode;
import com.yabby.htmlparser.rule.TagRuleItem;
import com.yabby.htmlparser.rule.TagWithAttributeRuleItem;
import org.junit.Test;

import java.io.InputStream;

import static org.junit.Assert.assertEquals;

public class ParserTest {

    private Parser parser = new JsoupParser();
//    private Parser parser = new NekoParser();
//    private Parser parser = new JerichoParser();

    private int counter;

    private final Action countAction = new Action() {
        @Override
        public void action(HtmlNode node) {
            counter++;
        }
    };

    @Test
    public void testSimpleFirst() throws Exception {
        counter = 0;
        // html/*/div
        parse("/simple/first.html", new Rule(countAction, new TagRuleItem("html"), new AnyNode(), new TagRuleItem("div")));
        assertEquals(6, counter);

        counter = 0;
        // */div[class='first']
        parse("/simple/first.html", new Rule(countAction, new AnyNode(), new TagWithAttributeRuleItem("div", "class", "first")));
        assertEquals(1, counter);

        counter = 0;
        // */div[id='second']/div
        parse("/simple/first.html", new Rule(countAction, new AnyNode(), new TagWithAttributeRuleItem("div", "id", "second"),
                new TagRuleItem("div")));
        assertEquals(3, counter);
    }

    @Test
    public void testSimpleSecond() throws Exception {
        counter = 0;
        parse("/simple/second.html", new Rule(countAction, new TagRuleItem("html"), new AnyNode(), new TagRuleItem("img")));
        assertEquals(1, counter);
    }

    @Test
    public void testRealBing() throws Exception {
        counter = 0;
        parse("/real/bing.html", new Rule(countAction, new AnyNode(), new TagRuleItem("div")));
        parse("/real/bing.html", new Rule(countAction, new AnyNode(), new TagRuleItem("table"), new AnyNode(), new TagRuleItem("td")));
        parse("/real/bing.html", new Rule(countAction, new AnyNode(), new TagWithAttributeRuleItem("div", "class", "itm_desc")));
        parse("/real/bing.html", new Rule(countAction, new AnyNode(), new TagRuleItem("ul"), new TagRuleItem("li")));
        parse("/real/bing.html", new Rule(countAction, new AnyNode(), new TagRuleItem("script")));
        assertEquals(121, counter);
        System.out.println("Counter: " + counter);
    }

    @Test
    public void testRealFacebook() throws Exception {
        counter = 0;
        parse("/real/facebook.html", new Rule(countAction, new AnyNode(), new TagRuleItem("div")));
        parse("/real/facebook.html", new Rule(countAction, new AnyNode(), new TagRuleItem("table"), new AnyNode(), new TagRuleItem("td")));
        parse("/real/facebook.html", new Rule(countAction, new AnyNode(), new TagWithAttributeRuleItem("div", "class", "mbm")));
        parse("/real/facebook.html", new Rule(countAction, new AnyNode(), new TagRuleItem("form"), new TagRuleItem("input")));
        parse("/real/facebook.html", new Rule(countAction, new AnyNode(), new TagRuleItem("script")));
        assertEquals(140, counter);
        System.out.println("Counter: " + counter);
    }

    @Test
    public void testRealGoogle() throws Exception {
        counter = 0;
        parse("/real/google.html", new Rule(countAction, new AnyNode(), new TagRuleItem("div")));
        parse("/real/google.html", new Rule(countAction, new AnyNode(), new TagRuleItem("table"), new AnyNode(), new TagRuleItem("td")));
        parse("/real/google.html", new Rule(countAction, new AnyNode(), new TagWithAttributeRuleItem("div", "class", "gb_0a")));
        parse("/real/google.html", new Rule(countAction, new AnyNode(), new TagRuleItem("form"), new TagRuleItem("input")));
        parse("/real/google.html", new Rule(countAction, new AnyNode(), new TagRuleItem("script")));
        assertEquals(153, counter);
        System.out.println("Counter: " + counter);
    }

    @Test
    public void testRealGoogleAccounts() throws Exception {
        counter = 0;
        parse("/real/google_accounts.html", new Rule(countAction, new AnyNode(), new TagRuleItem("div")));
        parse("/real/google_accounts.html", new Rule(countAction, new AnyNode(), new TagRuleItem("table"), new AnyNode(), new TagRuleItem("td")));
        parse("/real/google_accounts.html", new Rule(countAction, new AnyNode(), new TagWithAttributeRuleItem("form", "id", "gaia_loginform")));
        parse("/real/google_accounts.html", new Rule(countAction, new AnyNode(), new TagRuleItem("ul"), new TagRuleItem("li")));
        parse("/real/google_accounts.html", new Rule(countAction, new AnyNode(), new TagRuleItem("script")));
        assertEquals(30, counter);
        System.out.println("Counter: " + counter);
    }

    @Test
    public void testRealGoogleplus() throws Exception {
        counter = 0;
        parse("/real/googleplus.html", new Rule(countAction, new AnyNode(), new TagRuleItem("span")));
        parse("/real/googleplus.html", new Rule(countAction, new AnyNode(), new TagRuleItem("table"), new AnyNode(), new TagRuleItem("td")));
        parse("/real/googleplus.html", new Rule(countAction, new AnyNode(), new TagWithAttributeRuleItem("div", "class", "wvc")));
        parse("/real/googleplus.html", new Rule(countAction, new AnyNode(), new TagRuleItem("ul"), new TagRuleItem("li")));
        parse("/real/googleplus.html", new Rule(countAction, new AnyNode(), new TagRuleItem("script")));
        assertEquals(516, counter);
        System.out.println("Counter: " + counter);
    }

    @Test
    public void testRealGoogleplusAuthor() throws Exception {
        counter = 0;
        parse("/real/googleplus_author.html", new Rule(countAction, new AnyNode(), new TagRuleItem("span")));
        parse("/real/googleplus_author.html", new Rule(countAction, new AnyNode(), new TagRuleItem("table"), new AnyNode(), new TagRuleItem("td")));
        parse("/real/googleplus_author.html", new Rule(countAction, new AnyNode(), new TagWithAttributeRuleItem("div", "class", "hpc")));
        parse("/real/googleplus_author.html", new Rule(countAction, new AnyNode(), new TagRuleItem("ul"), new TagRuleItem("li")));
        parse("/real/googleplus_author.html", new Rule(countAction, new AnyNode(), new TagRuleItem("script")));
        assertEquals(29, counter);
        System.out.println("Counter: " + counter);
    }

    @Test
    public void testRealGoogleplusCommunity() throws Exception {
        counter = 0;
        parse("/real/googleplus_community.html", new Rule(countAction, new AnyNode(), new TagRuleItem("span")));
        parse("/real/googleplus_community.html", new Rule(countAction, new AnyNode(), new TagRuleItem("table"), new AnyNode(), new TagRuleItem("td")));
        parse("/real/googleplus_community.html", new Rule(countAction, new AnyNode(), new TagWithAttributeRuleItem("div", "class", "gb_B")));
        parse("/real/googleplus_community.html", new Rule(countAction, new AnyNode(), new TagRuleItem("ul"), new TagRuleItem("li")));
        parse("/real/googleplus_community.html", new Rule(countAction, new AnyNode(), new TagRuleItem("script")));
        assertEquals(686, counter);
        System.out.println("Counter: " + counter);
    }

    @Test
    public void testRealGoogleplusMobile() throws Exception {
        counter = 0;
        parse("/real/googleplus_mobile.html", new Rule(countAction, new AnyNode(), new TagRuleItem("span")));
        parse("/real/googleplus_mobile.html", new Rule(countAction, new AnyNode(), new TagRuleItem("table"), new AnyNode(), new TagRuleItem("td")));
        parse("/real/googleplus_mobile.html", new Rule(countAction, new AnyNode(), new TagWithAttributeRuleItem("div", "class", "okSfS")));
        parse("/real/googleplus_mobile.html", new Rule(countAction, new AnyNode(), new TagRuleItem("ul"), new TagRuleItem("li")));
        parse("/real/googleplus_mobile.html", new Rule(countAction, new AnyNode(), new TagRuleItem("script")));
        assertEquals(163, counter);
        System.out.println("Counter: " + counter);
    }

    @Test
    public void testRealLivejournal() throws Exception {
        counter = 0;
        parse("/real/livejournal.html", new Rule(countAction, new AnyNode(), new TagRuleItem("span")));
        parse("/real/livejournal.html", new Rule(countAction, new AnyNode(), new TagRuleItem("table"), new AnyNode(), new TagRuleItem("td")));
        parse("/real/livejournal.html", new Rule(countAction, new AnyNode(), new TagWithAttributeRuleItem("div", "class", "b-loginform-row")));
        parse("/real/livejournal.html", new Rule(countAction, new AnyNode(), new TagRuleItem("ul"), new TagRuleItem("li")));
        parse("/real/livejournal.html", new Rule(countAction, new AnyNode(), new TagRuleItem("script")));
        assertEquals(121, counter);
        System.out.println("Counter: " + counter);
    }

    @Test
    public void testRealYoutube() throws Exception {
        counter = 0;
        parse("/real/youtube.html", new Rule(countAction, new AnyNode(), new TagRuleItem("span")));
        parse("/real/youtube.html", new Rule(countAction, new AnyNode(), new TagRuleItem("table"), new AnyNode(), new TagRuleItem("td")));
        parse("/real/youtube.html", new Rule(countAction, new AnyNode(), new TagWithAttributeRuleItem("span", "class", "thumb")));
        parse("/real/youtube.html", new Rule(countAction, new AnyNode(), new TagRuleItem("ul"), new TagRuleItem("li")));
        parse("/real/youtube.html", new Rule(countAction, new AnyNode(), new TagRuleItem("script")));
        assertEquals(1084, counter);
        System.out.println("Counter: " + counter);
    }

    public void parse(String fileName, Rule... rules) throws Exception {
        InputStream stream = getClass().getResourceAsStream(fileName);
        YabbyHtmlParser yabbyHtmlParser = new YabbyHtmlParser(this.parser);
        for (Rule rule : rules) {
            yabbyHtmlParser.addRule(rule);
        }
        yabbyHtmlParser.parse(stream);
        stream.close();
    }
}
